### A Simpler Graph

The example project 'Simple Graph' shows how easy it is to use a ClearWin+ 
[%gr](https://www.silverfrost.com/ftn95-help/clearwinp/formats/_gr.aspx) graphics area to draw an updatable graph. This example shows that it
can be even easier by using the [%pl](https://www.silverfrost.com/ftn95-help/clearwinp/formats/_pl.aspx) graph format code.

![Sample](https://gitlab.com/silverfrost/clearwin-examples/-/raw/master/Simpler%20Graph/Simpler%20Graph.png)
