module graph_data
    integer, parameter :: data_points = 100
    double precision cosine_factor, frequency, freq
    double precision x(data_points), y(data_points)

contains

    subroutine fill_arrays()
      do i=1,data_points
        x(i) = (8*3.1415926d0 / data_points) * (i-1) ! 8pi spread amongst the data_points
        y(i) = sin(x(i)) + cosine_factor*cos(frequency*x(i))
      enddo
      freq = frequency
    end subroutine fill_arrays
    
end module

PROGRAM main
    use clrwin
    use graph_data    
    integer e
    integer redraw
    external redraw    

    cosine_factor = 0.5
    frequency = 2
    call fill_arrays
    
    e=winio@('%ca[Simpler graph]&')
    e=winio@('Cosine Multiplier: %^rf  Cosine Frequency: %^20sl&', cosine_factor, redraw, frequency, -5D0, 10D0, redraw)
    e=winio@(' %`rf&', freq)
    e=winio@('%nl&')
    ! use a %pl next to a %pv pivot. That way the %pl can expand when the window is resized
    ! here we use the 'smoothing=4' option to make the curve anti-aliased
    e=winio@('%pv%pl[native,x_array,smoothing=4]', 800, 350, data_points, x, y)

end program main

integer function redraw()
    use clrwin
    use graph_data    
    call fill_arrays
    call simpleplot_redraw@()

    call window_update@(freq)
    redraw = 1
end function
      