# ClearWin+ Examples

These are a set of examples that illustrating features of ClearWin+. This code can be reused and reworked as 
required. ClearWin+ is a UI framework used with [Silverfrost FTN95](https://www.silverfrost.com).

**Controls Demo** Shows off a range of the controls available in ClearWin+ ... and how to use them.

**Simple Graph** uses a [%gr](https://www.silverfrost.com/ftn95-help/clearwinp/formats/_gr.aspx) format and draws the lines 
onto the drawing surface using [DRAW_LINE_BETWEEN@](https://www.silverfrost.com/ftn95-help/clearwinp/library/draw_line_between_.aspx).
This simple example here could be developed as desired using ClearWin's extensive [graphics routines](https://www.silverfrost.com/ftn95-help/clearwinp/overview/graphicsfunctions_lines_fill_.aspx).

**Simpler Graph** goes one stage simpler than `Simple Graph` at least in terms of the code required. This uses the powerful [%pl](https://www.silverfrost.com/ftn95-help/clearwinp/formats/_pl.aspx) 
graph format code which is especially designed to allow simple access to powerful plots.

**Trivial Graph** takes plotting graphs to the extreme: one line of code. The graph is drawn using a single call to the SIMDEM routine gks001. SIMDEM is included as standard with FTN95 
and is automatically linked as required. Although SIMDEM is not part of ClearWin its graphics make extensive use of it.
