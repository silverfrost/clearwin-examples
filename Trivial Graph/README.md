### Trivial Graph

There are two ClearWin+ examples showing how simple it is to draw graphs using ClearWin+. This
example takes it to another level. The graph is drawn with one line of code and it makes use
of the SIMDEM library  that is included with FTN95. SIMDEM is a treasure trove of useful
routines.

![Sample](https://gitlab.com/silverfrost/clearwin-examples/-/raw/master/Trivial%20Graph/Trivial%20Graph.png)