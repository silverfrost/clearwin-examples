module graph_data
    integer, parameter :: data_points = 100
    double precision cosine_factor, frequency, freq
    double precision x(data_points), y(data_points)

contains

    subroutine fill_arrays()
      do i=1,data_points
        x(i) = (8*3.1415926d0 / data_points) * (i-1) ! 8pi spread amongst the data_points
        y(i) = sin(x(i)) + cosine_factor*cos(frequency*x(i))
      enddo
      freq = frequency
    end subroutine fill_arrays
    
end module

program main

    use graph_data
    cosine_factor = 0.5
    frequency = 2
    call fill_arrays
    call gks001 (1, 0, data_points, x, y, 'y = sin(x) + kcos(mx)', 'x', 'y')

end program main