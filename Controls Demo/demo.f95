WINAPP
PROGRAM demo
  CHARACTER(*),PARAMETER::fmt="%ff%dy%^28bb@&"
  REAL(KIND(1d0)),PARAMETER::dy=0.5d0
  INTEGER iw,winio@
  INTEGER,EXTERNAL::exHw,exMs,exDi,exIm,exRb,exLb,exMn,exTv,exGr,exGp,exSl,exPr,exTb,exTr,exLv
  iw = winio@("%ww&")
  iw = winio@("%ca[ClearWin+ demo]&")
  iw = winio@("%mi[clrwin]&")
  iw = winio@(fmt,dy,"Hello world!                 ",exHw)
  iw = winio@(fmt,dy,"Message box                  ",exMs)
  iw = winio@(fmt,dy,"Data input and output        ",exDi)
  iw = winio@(fmt,dy,"Images                       ",exIm)
  iw = winio@(fmt,dy,"Check boxes and Radio buttons",exRb)
  iw = winio@(fmt,dy,"List boxes and Combo boxes   ",exLb)
  iw = winio@(fmt,dy,"Menus and Accelerator keys   ",exMn)
  iw = winio@(fmt,dy,"Tab view                     ",exTv)
  iw = winio@(fmt,dy,"Graphics region              ",exGr)
  iw = winio@(fmt,dy,"Graph plotting               ",exGp)
  iw = winio@(fmt,dy,"Slider                       ",exSl)
  iw = winio@(fmt,dy,"Progress bar                 ",exPr)
  iw = winio@(fmt,dy,"Toolbar                      ",exTb)
  iw = winio@(fmt,dy,"Tree view                    ",exTr)
  iw = winio@(fmt,dy,"List view                    ",exLv)
  iw = winio@(" ")
END PROGRAM demo
!===============================================================================================
!Hello world
INTEGER FUNCTION exHw()
  INTEGER iw,winio@
  iw = winio@("Hello world!")
  exHw = 2
END FUNCTION exHw
!===============================================================================================
!Message Box
INTEGER FUNCTION exMs()
  INTEGER iw,winio@
  iw = winio@("%ca[ClearWin+]&")
  iw = winio@("Here is a sample message.&")
  iw = winio@("%fn[Verdana]&")
  iw = winio@("%ts&",0.90d0)
  iw = winio@("%ff%nl%cn&")
  iw = winio@("%6bb[OK]&")
  iw = winio@("  &")
  iw = winio@("%6bb[Cancel]")
  !Return value for iw...
  iw = winio@("winio@ returns the value: %wd",iw)
  exMs = 2
END FUNCTION exMs
!===============================================================================================
!Data input and output
INTEGER FUNCTION exDi()
  INTEGER n,iw,winio@
  REAL(KIND(1d0)) x
  n = 42
  x = 10d0
  iw = winio@("%ca[ClearWin+]&")
  iw = winio@("%~il&",1,256)
  iw = winio@("%tc[blue]&")
  iw = winio@("Integer Input:%ta%12rd&",n)
  iw = winio@("  Result: %`12rd&",n)
  iw = winio@("%ff&")
  iw = winio@("%fl&",0d0,100d0)
  iw = winio@("Real Input:%ta%12rf&",x)
  iw = winio@("  Result: %`12rf&",x)
  iw = winio@("%ff")
  exDi = 2
END FUNCTION exDi
!===============================================================================================
!Images
INTEGER FUNCTION exIm()
  INTEGER iw,winio@
  iw = winio@("%ca[ClearWin+]&")
  iw = winio@("%5.1ob&")
  iw = winio@("%?si*[Standard icon]&")
  iw = winio@("%cb&")
  iw = winio@("%?bm[bitmap1][Bitmap]&")
  iw = winio@("%cb&")
  iw = winio@("%?ic[icon1][Icon]&")
  iw = winio@("%cb&")
  iw = winio@("%?im[image1][JPEG image]&")
  iw = winio@("%cb&")
  iw = winio@("%?im[image2][PNG image]&")
  iw = winio@("%ff %cb")
  exIm = 2
END FUNCTION exIm
!===============================================================================================
!Check boxes and Radio buttons
INTEGER FUNCTION exRb()
  INTEGER iw,winio@,RGB@,ir(2),ic(2)
  ir(1)=1;ir(2)=0;ic(1)=1;ic(2)=0
  iw = winio@("%ca[ClearWin+]&")
  iw = winio@("%2.1ob[line_colour]&",RGB@(208,208,208))
  iw = winio@("%rb[Radio 1]&",ir(1))
  iw = winio@("%ff%nl&")
  iw = winio@("%rb[Radio 2]&",ir(2))
  iw = winio@("%2~ga&",ir)
  iw = winio@("%cb&")
  iw = winio@("%`rb[Check 1]&",ic(1))
  iw = winio@("%ff%nl&")
  iw = winio@("%`rb[Check 2]&",ic(2))
  iw = winio@("%cb")
  exRb = 2
END FUNCTION exRb
!===============================================================================================
!List boxes and Combo boxes
INTEGER FUNCTION exLb()
  INTEGER k1,k2,iw,winio@
  INTEGER,PARAMETER::N=7
  INTEGER,EXTERNAL::refresh
  CHARACTER(12) fruit(N)
  DATA fruit /'Apples','Bananas','Cherries','Grapes','Oranges','Pears','Raspberries'/
  k1 = 1; k2 = 1
  iw = winio@("%ca[ClearWin+]&")
  iw = winio@("%^ls&",fruit,N,k1,refresh)
  iw = winio@("Result: k1 = %`rd&",k1)
  iw = winio@("%ff%2nl&")
  iw = winio@("%`ls&",fruit,N,k2)
  iw = winio@("Result: k2 = %`rd", k2)
  exLb = 2
END FUNCTION exLb
!-----------------------------------------------------------------------------------------------
INTEGER FUNCTION refresh()
!Refresh the display.
  refresh = 1
END FUNCTION refresh
!===============================================================================================
!Menus and Accelerator keys
INTEGER FUNCTION exMn()
  INTEGER iw,winio@,check,enable
  CHARACTER(20) str
  INTEGER,EXTERNAL::cb7a,cb7b,cb7c,refresh
  enable = 1;check = 0;str = " "
  iw = winio@("%ca[ClearWin+]&")
  !Main menu...
  iw = winio@("%?mn[File[Exit]][Close it]&","EXIT")
  iw = winio@("%?mn[Edit[Cut]][Cut it]&",cb7a)
  iw = winio@("%?mn[[~Copy]][Copy it]&",enable,cb7b)
  iw = winio@("%?mn[[#Set]][Set it]&",check,"TOGGLE",check)
  !General popup menu...
  iw = winio@("%pm[Cut-pm,~Copy-pm, Paste-pm]&",cb7a,enable,cb7b,cb7c)
  !Popup menu for the next control...
  iw = winio@("%cm[Cut-cm,~Copy-cm,Paste-cm]&",cb7a,enable,cb7b,cb7c)
  iw = winio@("%cn%16rs&",str)
  iw = winio@("%ff%nl%cn%^rb[Enable Copy]&",enable,refresh)
  iw = winio@("%ff%nl%cn%6bb[OK]&")
  !Accelerator keys...
  iw = winio@("%ac[Ctrl+X]&",cb7a)
  iw = winio@("%ac[Ctrl+C]&",cb7b)
  iw = winio@("%ac[Ctrl+V]&",cb7c)
  iw = winio@("%`sb")
  exMn = 2
END FUNCTION exMn
!-----------------------------------------------------------------------------------------------
SUBROUTINE display(msg)
  CHARACTER(*) msg
  INTEGER iw,winio@
  iw = winio@("%ww[toolwindow]&")
  iw = winio@("%ca[ClearWin+]&")
  iw = winio@("%cn%ws&",msg)
  iw = winio@("%ff%nl%cn%6bb[OK]")
END SUBROUTINE 
!-----------------------------------------------------------------------------------------------
INTEGER FUNCTION cb7a();CALL display("Cut");  cb7a = 2; END
INTEGER FUNCTION cb7b();CALL display("Copy"); cb7b = 2; END
INTEGER FUNCTION cb7c();CALL display("Paste");cb7c = 2; END
!===============================================================================================
!Tab view... 
INTEGER FUNCTION exTv()
  INTEGER iw,winio@,wh(2)
  CHARACTER(1024) buffer1,buffer2
  buffer1 = "Type here..."; buffer2 = "Type more here... "
  !First property sheet
  iw = winio@("%sh%ca[Sheet 1]&",wh(1))
  iw = winio@("%fn[Verdana]%ts[0.9]&")
  iw = winio@("%`bg[White]&")
  iw = winio@("%30.5re",buffer1)
  !Next property sheet
  iw = winio@("%sh%ca[Sheet 2]&",wh(2))
  iw = winio@("%fn[Verdana]%ts[0.9]&")
  iw = winio@("%`bg[White]&")
  iw = winio@("%30.5re",buffer2)
  !Display the combined sheets
  iw = winio@("%ca[ClearWin+]&")
  iw = winio@("%bg[BTNFACE]&")
  iw = winio@("%~2ps[hot_track]&",wh)
  iw = winio@("%fn[Verdana]%ts[0.9]&")
  iw = winio@("%ff%nl%cn%6bb[Exit]")
  exTv = 2
END FUNCTION exTv
!===============================================================================================
!Graphics region...
MODULE grData
  USE clrwin
  CHARACTER(32) cstat
CONTAINS
  INTEGER FUNCTION gr_func()
    INTEGER x1,y1,x2,y2,nstat,a,b
    INTEGER,PARAMETER::MK_SHIFT=4,MK_CONTROL=8,RED=255
    CALL get_mouse_info@(x1,y1,nstat)
    write(cstat(1:30),'(3I7)') x1,y1,nstat
    CALL window_update@(cstat)
    IF(clearwin_string@('CALLBACK_REASON') == 'MOUSE_LEFT_CLICK')THEN
      CALL set_line_width@(2)
      CALL get_graphics_selected_area@(x1,y1,x2,y2)
      CALL set_graphics_selection@(0)
      IF(iand(nstat,MK_SHIFT)==MK_SHIFT) THEN
        CALL draw_rectangle@(x1,y1,x2,y2,RED)
      ELSE IF(iand(nstat,MK_CONTROL)==MK_CONTROL) THEN
        a=0.5*(x2-x1)
        b=0.5*(y2-y1)
        CALL draw_ellipse@(x1+a,y1+b,a,b,RED)
      ELSE
        CALL draw_line_between@(x1,y1,x2,y2,RED)
      ENDIF
      CALL set_graphics_selection@(1)
    ENDIF
    gr_func=2
  END FUNCTIOn
  INTEGER FUNCTION help()
    INTEGER iw
    iw = winio@('%sy[3d]%ca[Instructions]&')
    iw = winio@('%fn[Arial]%ts&',0.95D0)
    iw = winio@('Click, drag and release to draw a line.%2nl&')
    iw = winio@('Also hold down the SHIFT key to draw a rectangle.%2nl&')
    iw = winio@('Alternatively hold down the CONTROL key to draw an ellipse.%2nl&')
    iw = winio@('%cn%tt[OK]')
    help=2
  END FUNCTION
END MODULE grData
!-----------------------------------------------------------------------------------------------
INTEGER FUNCTION exGr()
  USE grData
  INTEGER iw
  cstat=' '
  iw = winio@('%ww&')
  iw = winio@('%ca[ClearWin+]&')
  iw = winio@('%mn[E&xit,Help]&','EXIT',help) 
  iw = winio@('%bg&',RGB@(240,240,240))
  iw = winio@('%ob&')
  iw = winio@('%^gr[colour=#f0f0f0,box_selection,smooth8]&',300,300,gr_func)
  iw = winio@('%cb&')
  iw = winio@('%ff %nl&')
  iw = winio@('%ob[status]%20st%cb',cstat)
  exGr = 2
END FUNCTION exGr
!===============================================================================================
!Graph plotting...
MODULE plData
  USE clrwin
  INTEGER(7) hwnd
CONTAINS
  INTEGER FUNCTION cb()
  CHARACTER(80) status,reason
  REAL(KIND(1d0)) x,y
  INTEGER i,ix,iy
  reason = clearwin_string@("CALLBACK_REASON")
  IF(reason == "MOUSE_MOVE")THEN
    ix = clearwin_info@("GRAPHICS_MOUSE_X")
    iy = clearwin_info@("GRAPHICS_MOUSE_Y")
    i = get_plot_data@(ix,iy,x,y)
    status = " "
    IF(x >= 0d0 .AND. y >= 0d0 .AND. x <= 1d0 .AND. y <= 1d0)THEN
      write(status,"(a,i3,a,i3,a)") ' Point(',ix,',',iy,')'
      CALL set_status_text@(hwnd,0,status)
      write(status,"(a,f6.2,a,f6.2,a)") ' Value(',x,',',y,')'
    ELSE
      CALL set_status_text@(hwnd,0,status)
    ENDIF          
    CALL set_status_text@(hwnd,1,status)
  ENDIF  
  cb = 2
  END FUNCTION
END MODULE plData
!-----------------------------------------------------------------------------------------------
INTEGER FUNCTION exGp()
  USE plData
  INTEGER,PARAMETER:: N=11,M=2
  REAL(KIND(1d0)) x(N),y(N)
  INTEGER i,iw,sbparts(M)
  DATA sbparts /26,-1/
  DO i=1,N
    x(i)=0.1d0*(i-1)
    y(i)=x(i)*x(i) 
  ENDDO
  iw = winio@('%ww%ca[ClearWin+]&')
  iw = winio@('%bg[BTNFACE]&')
  CALL winop@("%pl[title=Quadratic]") 
  CALL winop@("%pl[width=2]")     
  CALL winop@("%pl[x_array]")     
  CALL winop@("%pl[link=curves]") 
  CALL winop@("%pl[symbol=9]")    
  CALL winop@("%pl[colour=red]")  
  CALL winop@("%pl[pen_style=2]") 
  CALL winop@("%pl[frame,gridlines]")
  CALL winop@("%pl[y_sigfigs=2]")
  CALL winop@("%pl[y_axis=y-data]")
  CALL winop@("%pl[x_max=1.0]")
  CALL winop@("%pl[y_max=1.0]")
  CALL winop@("%pl[dx=0.2]")
  CALL winop@("%pl[dy=0.2]")
  CALL winop@("%pl[full_mouse_input]")
  iw = winio@('%`bg[white]&')
  iw = winio@('%`*sb%lc&', M, sbparts, hwnd)
  iw = winio@('%`bg&',RGB@(252,252,252))
  iw = winio@('%pv%^pl',400,250,N,x,y,cb)
  exGp = 2
END FUNCTION exGp
!===============================================================================================
!Slider...
INTEGER FUNCTION exSl()
INTEGER iw,winio@,RGB@
 REAL(KIND(1d0)) value,lower,upper
 INTEGER val,low,high
 DATA value,lower,upper/5.0D0,0.0D0,10.0D0/
 DATA val,low,high/5,0,10/
 iw = winio$('%ww%ca[ClearWin+]&')
 iw = winio@('%bg&',RGB@(240,240,240))
 iw = winio@('%ob&')
 iw = winio$('%cn%30sl&',value,lower,upper)
 iw = winio@('%cb&')
 iw = winio$('%ff%nl%cn&')
 iw = winio$('Result: %`rf&',value)
 iw = winio@('%ff%nl&')
 iw = winio@('%cn%30sl[border,auto_ticks,tooltips=top]&',val,low,high)
 iw = winio@(' ')
 exSl = 2
END FUNCTION exSl
!===============================================================================================
!Progress bar...
MODULE prData
 USE clrwin
 REAL(KIND(1d0)) d
CONTAINS
 INTEGER FUNCTION start()
  DO WHILE(d < 1.0D0)
    CALL sleep1@(0.1)
    d = d+0.01
    CALL window_update@(d) 
  END DO
  d = 0d0
  start = 2
 END FUNCTION
END MODULE prData
!-----------------------------------------------------------------------------------------------
INTEGER FUNCTION exPr()
USE prData
INTEGER iw,thCtrl
 d = 0d0;thCtrl = 1
 iw = winio@('%ca[ClearWin+]&')
 iw = winio@('%20br[percentage]&',d,RGB@(255,0,0))
 iw = winio@('%ff%nl%cn&')
 iw = winio@('%th[balloon]&',thCtrl)
 iw = winio@('%^?6bb[Start][|* |Click to start]',start)
 exPr = 2
END FUNCTION exPr
!===============================================================================================
!Tool bar...
MODULE tbData
  USE mswin
  INTEGER,PARAMETER::N=9,UNITN=7
  INTEGER count
  INTEGER bstate(N)
  INTEGER(7) mbCtrl
CONTAINS
INTEGER function cb()
  CHARACTER(80) reason
  INTEGER buttonNo,iw
  count = count+1
  cb = 2
  reason = clearwin_string@("CALLBACK_REASON")
  buttonNo = clearwin_info@("MB_BUTTON")
  SELECT CASE(reason)
    CASE("MB_CUSTOMISE?")
       cb = 1 !Allow Customise dialog
    CASE("MB_INSERT?")
       cb = 1
    CASE("MB_DELETE?")
       cb = 1
       if(buttonNo == 2) cb = 0  !Don't allow button 2 to be deleted.
    CASE("MB_COMMAND")   
       if(buttonNo == 3)then
         bstate(3) = iand(bstate(3),not(TBSTATE_ENABLED)) !Disable button 3 after first click
         call window_update@(bstate)
       endif
    CASE("MB_DROPDOWN?")
       iw = winio@("%nw&",mbCtrl)
       iw = winio@("%pm[Item 1,Item 2,Exit]",cb1,cb2,"Exit")
       cb = 0 !TBDDRET_DEFAULT - use this to suppress the command when not using %mb[arrows]
    CASE("MB_HELP")
       iw = MessageBox(0_7, "Help requested", "Multi-button", MB_ICONQUESTION)   
  END SELECT
  WRITE(UNITN,"(i6,a16,2i6)") count,trim(reason),buttonNo,cb
END FUNCTION cb
!-----------------------------------------------------------------------------------------------
INTEGER FUNCTION cb1()
  count = count+1
  WRITE(UNITN,"(i6,a16,2i6)") count,"MENU_SELECTION",101,cb1
  cb1 = 2
END FUNCTION cb1
!-----------------------------------------------------------------------------------------------
INTEGER FUNCTION cb2()
  count = count+1
  WRITE(UNITN,"(i6,a16,2i6)") count,"MENU_SELECTION",102,cb2
  cb2 = 2
  END FUNCTION cb2
!-----------------------------------------------------------------------------------------------
INTEGER FUNCTION help()
  INTEGER iw
  iw = winio@('%ca[Instructions]&')
  iw = winio@('%fn[Verdana]%ts&',0.9D0)
  iw = winio@('� Double click on the toolbar to customise it.%2nl&')
  iw = winio@('� Press the shift key and drag to move or remove a button.%2nl&')
  iw = winio@('� When closing you will get the option to restart.%2nl&')
  iw = winio@('%cn%tt[OK]')
  help = 2
END FUNCTION help  

END MODULE tbData
!-----------------------------------------------------------------------------------------------
INTEGER FUNCTION exTb()
  USE tbData
  INTEGER,PARAMETER::TBSTYLE_DROPDOWN = 8 
  CHARACTER(*),PARAMETER::helpStr = "|Help 1|Help 2|Help 3||Help 5|Help 6||Help 8|Help 9" 
  INTEGER iw,thCtrl
  INTEGER bstyle(N),bID(N),b_order(N)
  count = 0;thCtrl = 1
  bID       = (/0,1,2,-1,3,4,-1,5,6/)
  bstyle    = TBSTYLE_BUTTON
  bstyle(6) = TBSTYLE_DROPDOWN
  bstyle(4) = TBSTYLE_SEP
  bstyle(7) = TBSTYLE_SEP
  bstate    = TBSTATE_ENABLED
  b_order    = -1
  
  DO
    iw = winio@("%ww%ca[ClearWin+]&")
    iw = winio@("%*`~?^mb[adjustable,arrows,border]@&", N, bID, bstyle, "buttons", bstate, b_order, cb, helpStr)
    iw = winio@("%lc&", mbCtrl)
    iw = winio@("%fn[Courier New]%ts[0.8]&")
    iw = winio@("%nl   Count      Reason     Value  Rtn&")
    iw = winio@("%`bg[white]&")
    iw = winio@("%nl%pv%40.8cw[vscroll,local_font,border]&", UNITN) !For 32 bits, don't use zero for the Fortran UNIT.
    iw = winio@("%fn[Verdana]%ts[0.9]&")
    iw = winio@("%ff%nl%cn%^bb[Instructions]",help)
  !Restart or exit...  
    iw = winio@("%fn[Verdana]%ts[0.9]&")
    iw = winio@("%th[ms_style]&",thCtrl)
    iw = winio@("%cn%?6bb[Restart][Click to view the saved re-ordering of buttons]  %6bb[Exit]")
    if(iw /= 1) EXIT
  END DO
  exTb = 2
END FUNCTION exTb
!===============================================================================================
!Tree view...
INTEGER FUNCTION exTr()
  INTEGER,PARAMETER::N_ITEMS=10
  CHARACTER(30) contents(N_ITEMS)
  INTEGER item,iw,winio@
  DATA contents /'AEBBook', &
       'BCAChapter 1', 'CCASection 1.1', 'CCASection 1.2', &
       'BCAChapter 2', 'CCASection 2.1', 'CCASection 2.2', &
       'BCAChapter 3', 'CCASection 3.1', 'CCASection 3.2'/
  item = 1
  iw = winio@("%ww%pv&")
  iw = winio@("%ca[ClearWin+]&") 
  iw = winio@("%`bv[has_buttons,has_lines,lines_at_root,paired_bitmaps,24bits]", &
              200,160,contents,N_ITEMS,item,"closed,opened")
  exTr = 2
END FUNCTION exTr
!===============================================================================================
!List view...
MODULE lvData
 USE clrwin
 INTEGER,PARAMETER::N=20
 CHARACTER(36) info(N+1)
 INTEGER(7) hwnd
 INTEGER view,rgb(N),sel(N),next,row,ctrl,enableHelp
 CONTAINS
!-----------------------------------------------------------------------------------------------
  CHARACTER(36) FUNCTION Colour(name,rgb)
    CHARACTER(*) name
    INTEGER rgb,r,g,b
    r=IAND(255,rgb)
    g=IAND(255,ISHFT(rgb,-8))
    b=IAND(255,ISHFT(rgb,-16))
    write(Colour,'(a,i3,a,i3,a,i3)') name,r,'|',g,'|',b
  END FUNCTION
!-----------------------------------------------------------------------------------------------
  INTEGER FUNCTION InsertC()
    INTEGER NewRgb
    InsertC=2
    IF(next>N) RETURN
    NewRgb=choose_colour@()
    IF(NewRgb /= -1)THEN
      rgb(next)=NewRgb;
      info(next+1)=Colour('|GUser colour|',NewRgb)
      sel(row)=0
      sel(next)=1
      CALL window_update@(info)
      CALL set_control_back_colour@(hwnd,NewRgb)
      next=next+1
    ENDIF
  END FUNCTION
!-----------------------------------------------------------------------------------------------
  INTEGER FUNCTION DeleteC()
    INTEGER i
    DeleteC=2
    IF(row <= 6) RETURN
    next = next-1
    DO i=row,next
      rgb(i)=rgb(i+1)
      info(i+1)=info(i+2)
    END DO
    info(next+2)=' '
    i = row
    IF(i == next) i = i-1
    CALL set_control_back_colour@(hwnd,rgb(i))
    CALL window_update@(info)
  END FUNCTION
!-----------------------------------------------------------------------------------------------
  INTEGER FUNCTION call_back() 
    integer keycode,i,column,rgb1(N)
    CHARACTER(36) name,info1(N+1)
    call_back=2
    SELECT CASE(clearwin_string@('CALLBACK_REASON'))
    CASE('SET_SELECTION')
      row=clearwin_info@('ROW_NUMBER')
      call set_control_back_colour@(hwnd,rgb(row))
      ctrl=0
      if(row>6) ctrl=1
    CASE('MOUSE_DOUBLE_CLICK')
      i=ChangeC()
    CASE('BEGIN_EDIT')
      if(row<7) call_back=4
    CASE('END_EDIT')
      name=clearwin_string@('EDITED_TEXT')
      info(row+1)=Colour('|G'//name(1:LEN_TRIM(name))//'|',rgb(row))
      CALL window_update@(info)
    CASE('KEY_DOWN')
      keycode=clearwin_info@('KEYBOARD_KEY')
      IF(keycode==45)THEN
        i=InsertC()
      ELSEIF(keycode==46) THEN
        i=DeleteC()
      ENDIF
    CASE('COLUMN_CLICK')
      column=clearwin_info@('COLUMN_NUMBER')
      if(column/=1)RETURN
      rgb1=rgb
      info1=info
      IF(rgb(1)==RGB@(255,0,0))THEN
        rgb(1)=rgb1(3);rgb(2)=rgb1(4);rgb(3)=rgb1(2);
        rgb(4)=rgb1(5);rgb(5)=rgb1(1);
        info(2)=info1(4);info(3)=info1(5);info(4)=info1(3);
        info(5)=info1(6);info(6)=info1(2);
      ELSE
        rgb(1)=rgb1(5);rgb(2)=rgb1(3);rgb(3)=rgb1(1);
        rgb(4)=rgb1(2);rgb(5)=rgb1(4);
        info(2)=info1(6);info(3)=info1(4);info(4)=info1(2);
        info(5)=info1(3);info(6)=info1(5);
      ENDIF
      CALL window_update@(info)
    END SELECT
  END FUNCTION
!-----------------------------------------------------------------------------------------------
  INTEGER FUNCTION ChangeC()
    INTEGER NewRgb
    ChangeC=2
    IF(row <= 6) RETURN
    NewRgb=choose_colour@()
    IF(NewRgb /= -1)THEN
      rgb(row)=NewRgb;
      info(row+1)=Colour('|GUser colour|',NewRgb)
      CALL set_control_back_colour@(hwnd,NewRgb)
      CALL window_update@(info)
    ENDIF
  END FUNCTION
!-----------------------------------------------------------------------------------------------
  INTEGER FUNCTION closingHelp()
   enableHelp  = 1
   closingHelp = 0
  END FUNCTION
!-----------------------------------------------------------------------------------------------
  INTEGER FUNCTION help()
    INTEGER iw
    IF(enableHelp > 0)THEN
      enableHelp = 0
      iw = winio@('%ww%ca[Instructions]&')
      iw = winio@('%fn[Verdana]%ts&',0.9D0)
      iw = winio@('� Press the Insert key to add a new colour.%2nl&')
      iw = winio@('� Press the Delete key to delete a new colour.%2nl&')
      iw = winio@('� The label of a new colour can be edited.%2nl&')
      iw = winio@('� Double click on a new colour to change its%nl&')
      iw = winio@('  components and reset the name.%2nl&')
      iw = winio@('� You can also click on the right mouse button.%2nl&')
      iw = winio@('� Click on the header of the first column to%nl&')
      iw = winio@('  change the order.%2nl&')
      iw = winio@('� You can return to the main window and leave%nl&')
      iw = winio@('  leave this window open.%2nl&')
      iw = winio@('%cn%tt[OK]&')
      iw = winio@('%cc',closingHelp)
    ENDIF
    help=2
  END FUNCTION
      
END MODULE lvData
!-----------------------------------------------------------------------------------------------
INTEGER FUNCTION exLv()
  USE lvData
  INTEGER iw
  sel=0;rgb=0;enableHelp=1
  rgb(1)=RGB@(255,0,0)
  rgb(2)=RGB@(0,255,0)
  rgb(3)=RGB@(0,0,255)
  rgb(4)=RGB@(0,255,255)
  rgb(5)=RGB@(255,0,255) 
  rgb(6)=RGB@(255,255,0)
  info = ' '
  info(1)='|Colour_90|Red|Green|Blue'
  info(2)=Colour('|ARed|',     rgb(1))
  info(3)=Colour('|BGreen|',   rgb(2))
  info(4)=Colour('|CBlue|',    rgb(3)) 
  info(5)=Colour('|DCyan|',    rgb(4))
  info(6)=Colour('|EMagenta|', rgb(5))
  info(7)=Colour('|FYellow|',  rgb(6))
  view=1;next=7;row=0;ctrl=0
  iw = winio@('%ww%ca[ClearWin+]&')
  iw = winio@('%mn[Views[Icon]]&', 'SET',view,0)
  iw = winio@('%mn[[Report]]&',    'SET',view,1)
  iw = winio@('%mn[[Small icon]]&','SET',view,2)
  iw = winio@('%mn[[List]]&',      'SET',view,3)
  iw = winio@('%mn[Help[~Instructions]]&',enableHelp,help)
  iw = winio@('%pm[Add]&',InsertC)
  iw = winio@('%pm[~Delete]&',ctrl,DeleteC)
  iw = winio@('%pm[~Change]&',ctrl,ChangeC)
  iw = winio@('%pv%`^lv[edit_labels,single_selection]&',260,200,info,N, &
 &  sel,view,'red,green,blue,cyan,magenta,yellow,blank',call_back)
  iw = winio@('%ff%nl%cn%10`rs%lc','  Selected colour',hwnd)
  exLv = 2
END FUNCTION exLv
!===============================================================================================
RESOURCES
clrwin  ICON   clrwin.ico
bitmap1 BITMAP find1.bmp
icon1   ICON   red.ico
image1  IMAGE  sphero.jpg
image2  IMAGE  device.png
closed  BITMAP closeb.bmp
opened  BITMAP openb.bmp
red     ICON red.ico
green   ICON green.ico
blue    ICON blue.ico
cyan    ICON cyan.ico
magenta ICON magenta.ico
yellow  ICON yellow.ico
blank   ICON blank.ico
buttons BITMAP designToolbar.bmp
!===============================================================================================

