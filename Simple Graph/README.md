# Simple Graph example 

This shows how a fairly small amount of code when used with a [%gr](https://www.silverfrost.com/ftn95-help/clearwinp/formats/_gr.aspx) format code can produce an
interactive graph.

![Sample](https://gitlab.com/silverfrost/clearwin-examples/-/raw/master/Simple%20Graph/simple%20graph.png)

