module graph_data
! module to store shared data
    integer, parameter :: gutter = 20 ! pixel border
    integer width, height ! %gr size
    double precision cosine_factor !multiplier for the cosine bit of the graph
    double precision frequency !sine frequency
end module


PROGRAM main
    use clrwin
    use graph_data
    integer e
    integer redraw
    external graphics_callback, redraw
    

    cosine_factor = 0.5
    frequency = 2
    width = 800
    height = 350
    e=winio@('%ca[Simple graph]&')  ! caption
    e=winio@('Cosine Multiplier: %^rf  Cosine Frequency: %^20sl&', cosine_factor, redraw, frequency, -5D0, 10D0, redraw)
    e=winio@(' %`rf&', frequency) ! a text label where we can show the frequency
    e=winio@('%nl&')
    e=winio@('%pv%^gr[smooth4,user_resize]', width, height, graphics_callback) ! a graphics area

end program main

integer function redraw()
    use clrwin
    use graph_data
    integer last_y, mid_y
    integer new_y
    double precision scale, x
    mid_y = height / 2
    call clear_screen@
    ! draw axes
    call draw_line_between@(gutter, gutter, gutter, width-gutter, 0 )
    call draw_line_between@(gutter, mid_y, width-gutter, mid_y, 0 )
    scale = (mid_y-gutter) / 1.5

    do i = 0,width-(2*gutter)
        ! make the width of the x-axis span 0..8pi. REAL(width-(2*gutter) is the width of that axis
        x = (i/REAL(width-(2*gutter)))*8*3.1415926d0
        new_y = mid_y + sin(x)*scale + cosine_factor*cos(frequency*x)*scale !*exp(-x / 10)
        ! screens have zero at the top. Graphs have zero at the bottom, so invert for correct display
        new_y = height - new_y
        if (i > 0) then
            call draw_line_between@(gutter+i-1, last_y, gutter+i, new_y, RGB@(255,0,0) )
        end if
        ! Make sure the %`rf is updated
        call window_update@(frequency)
        ! Update the screen. The screen will update anyway but if we are in a slider drag this will force it
        call perform_graphics_update@()
        last_y = new_y
    end do
    redraw = 1
end function

integer function graphics_callback()
    use clrwin
    use graph_data
    integer e, redraw
    external redraw
    ! is this a resize?
    if (clearwin_info@('GRAPHICS_RESIZING').eq.1) then
        ! resize the graphic then redraw
        width=clearwin_info@('GRAPHICS_WIDTH')
        height=clearwin_info@('GRAPHICS_DEPTH')
        e=redraw()
    endif
    graphics_callback=1
end function